const localStrategy = require('passport-local').Strategy;
const User = require('./../models').user;
const bcrypt = require('bcryptjs');

module.exports = function (passport) {
    passport.use(
        new localStrategy((username, password, done) => {
            //Match user 
            User.findOne({ where: { username: username } }).then(u => {
                if (!u) return done(null, false, { message: `No user with username ${username}` });

                //Match password
                bcrypt.compare(password, u.password, (err, isMatch) => {
                    if (err) throw err;

                    if (isMatch) return done(null, u);
                    else return done(null, false, { message: `User not found!` });
                });

            }).catch(ex => {
                throw ex;
            });
        })
    );

    passport.serializeUser(function (user, done) {
        console.log("ser", user.id);
        done(null, user.id);
    });

    passport.deserializeUser(function (id, done) {
        console.log("deser", id);
        done(null, id);
    });
}
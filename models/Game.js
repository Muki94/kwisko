'use strict';

const moment = require('moment');

module.exports = (sequelize, DataTypes) => {
  const Game = sequelize.define('game', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID
    },
    gameName: DataTypes.STRING

  }, {});
  Game.associate = function (models) {
    // associations can be defined here
    Game.hasMany(models.gameQuestion);
    Game.hasMany(models.gameQuestionType);
    Game.hasMany(models.gameTeam);
    Game.belongsTo(models.user);
  };
  return Game;
};
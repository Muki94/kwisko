'use strict';
module.exports = (sequelize, DataTypes) => {
  const GameQuestion = sequelize.define('gameQuestion', {
    id:{
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID
    }
  }, {});
  GameQuestion.associate = function(models) {
    // associations can be defined here
    GameQuestion.belongsTo(models.game);
    GameQuestion.belongsTo(models.question);
  };
  return GameQuestion;
};
'use strict';
module.exports = (sequelize, DataTypes) => {
  const GameQuestionType = sequelize.define('gameQuestionType', {
    id:{
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID
    },
    numberOfQuestions: DataTypes.INTEGER
  }, {});
  GameQuestionType.associate = function(models) {
    // associations can be defined here
    GameQuestionType.belongsTo(models.game);
    GameQuestionType.belongsTo(models.questionType);
  };
  return GameQuestionType;
};
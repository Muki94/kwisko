'use strict';
module.exports = (sequelize, DataTypes) => {
  const GameTeam = sequelize.define('gameTeam', {
    id:{
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID
    },
    score: DataTypes.INTEGER,
    game_id: DataTypes.INTEGER
  }, {});
  GameTeam.associate = function(models) {
    // associations can be defined here
    GameTeam.belongsTo(models.game);
    GameTeam.belongsTo(models.team);
  };
  return GameTeam;
};
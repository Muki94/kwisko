'use strict';
module.exports = (sequelize, DataTypes) => {
  const Question = sequelize.define('question', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID
    },
    questionText: DataTypes.STRING,
    questionAnswer: DataTypes.STRING,
    questionNote: DataTypes.STRING
  }, {});
  Question.associate = function (models) {
    // associations can be defined here
    Question.belongsTo(models.questionType);
    Question.hasMany(models.gameQuestion);
    Question.belongsTo(models.user);
  };
  return Question;
};
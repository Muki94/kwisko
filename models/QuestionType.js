'use strict';
module.exports = (sequelize, DataTypes) => {
  const QuestionType = sequelize.define('questionType', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes
        .UUID
    },
    questionTypeName: DataTypes.STRING
  }, {});
  QuestionType.associate = function (models) {
    // associations can be defined here
    QuestionType.hasMany(models.question);
    QuestionType.hasMany(models.gameQuestionType);
  };
  return QuestionType;
};
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Team = sequelize.define('team', {
    id:{
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID
    },
    teamName: DataTypes.STRING
  }, {});
  Team.associate = function(models) {
    // associations can be defined here
    Team.hasMany(models.gameTeam);
  };
  return Team;
};
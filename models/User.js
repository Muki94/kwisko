'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    id:{
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID
    },
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};
const express = require('express');
const uuid = require('uuid/v4');

var router = express.Router();
const Op = require('./../models').Sequelize.Op;
const Game = require('./../models').game;
const Question = require('./../models').question;
const QuestionType = require('./../models').questionType;
const GameQuestion = require('./../models').gameQuestion;
const sequelize = require('./../models').sequelize;

const isUserLoged = require('./../helpers/user-auth-helper').isUserLoged;


/* #region  Handlebars */
router.get('/user/all', isUserLoged, function (req, res) {
    Game.findAll({
        attributes: ['id',
            'gameName',
            [sequelize.fn('TO_CHAR', sequelize.col('createdAt'), 'DD-MM-YYYY HH:mm:ss'), 'createdAt'],
            [sequelize.fn('TO_CHAR', sequelize.col('updatedAt'), 'DD-MM-YYYY HH:mm:ss'), 'updatedAt']],
        where: { userId: req.user }
    }).then(g => {
        res.render('./game/user_games', { games: g });
    }).catch(ex => {
        console.log(ex);
    });
});

router.get('/all', isUserLoged, function (req, res) {
    Game.findAll().then(g => {
        res.render('./game/index');
    }).catch(ex => {
        console.log(ex);
    });
});

router.get('/detail/:id', isUserLoged, function (req, res) {
    Game.findByPk(req.params.id)
        .then(g => {
            let questions = {};
            //daj sva pitanja za pronadjenu igru 

            GameQuestion.findAll({
                where: {
                    'gameId': g.id
                },
                include: [{ model: Question }]
            }).then(gq => {
                //za svaki pronadjen game question type izvrtiti questionId-jeve i dodati u zaseban niz
                //dodaj pitanja u jedan array kojeg i vraca 
                addExistingQuestionPromise(gq).then(existingQuestions => {
                    //izvuci idjeve izdvojenih pitanja 
                    takeExistingQuestionIdsPromise(existingQuestions).then(existingQuestionsIds => {
                        Question.findAll({ where: { 'id': { [Op.notIn]: existingQuestionsIds } } }).then(nonExistingQeustions => {
                            //izvuci sve question typove sa postojecim pitanjima 
                            QuestionType.findAll({
                                attributes: ['id', 'questionTypeName'],
                                include: [{
                                    model: Question,
                                    attributes: ['id',
                                        'questionText',
                                        'questionAnswer',
                                        'questionNote',
                                        [sequelize.fn('TO_CHAR', sequelize.col('questions.createdAt'), 'DD-MM-YYYY HH:mm:ss'), 'createdAt'],
                                        [sequelize.fn('TO_CHAR', sequelize.col('questions.updatedAt'), 'DD-MM-YYYY HH:mm:ss'), 'updatedAt']],
                                    where: {
                                        id: {
                                            [Op.in]: existingQuestionsIds
                                        }
                                    },
                                    required: false
                                }]
                            }).then(qtExistingQuestions => {
                                //izvuci sve question typove za ne postojeca pitanja
                                QuestionType.findAll({
                                    attributes: ['id', 'questionTypeName'],
                                    include: [{
                                        model: Question,
                                        attributes: ['id',
                                            'questionText',
                                            'questionAnswer',
                                            'questionNote',
                                            [sequelize.fn('TO_CHAR', sequelize.col('questions.createdAt'), 'DD-MM-YYYY HH:mm:ss'), 'createdAt'],
                                            [sequelize.fn('TO_CHAR', sequelize.col('questions.updatedAt'), 'DD-MM-YYYY HH:mm:ss'), 'updatedAt']],
                                        where: {
                                            id: {
                                                [Op.notIn]: existingQuestionsIds
                                            }
                                        },
                                        required: false
                                    }]
                                }).then(qtNonExistingQuestions => {
                                    res.render('./game/detail', { game: g, existingQuestions: qtExistingQuestions, nonExistingQuestions: qtNonExistingQuestions });
                                }).catch(ex => {
                                    console.log(ex);
                                });
                            }).catch(ex => {
                                console.log(ex);
                            });
                        })
                    });
                });
            }).catch(ex => {
                console.log(ex);
            });

        }).catch(ex => {
            console.log(ex);
        });
});

function addExistingQuestionPromise(gq) {
    var promise = new Promise(function (resolve, reject) {
        setTimeout(() => {
            let questionExist = [];

            for (let i = 0; i < gq.length; i++) {
                questionExist.push(gq[i].question);
            }

            resolve(questionExist);
        }, 300);
    });

    return promise;
}

function takeExistingQuestionIdsPromise(existingQuestions) {
    var promise = new Promise(function (resolve, reject) {
        setTimeout(() => {
            let questionExistIds = [];

            for (let i = 0; i < existingQuestions.length; i++) {
                questionExistIds.push(existingQuestions[i].id);
            }

            resolve(questionExistIds);
        }, 300);
    });

    return promise;
}

router.get('/add', isUserLoged, function (req, res) {
    res.render('./game/add');
});

router.post('/add', isUserLoged, function (req, res) {
    var newGame = {
        id: uuid(),
        gameName: req.body.gameName,
        userId: req.user
    };

    Game.create(newGame).then(r => {
        res.redirect("/game/user/all");
    }).catch(ex => {
        console.log(ex);
    });
});

router.get('/edit/:id', isUserLoged, function (req, res) {
    Game.findByPk(req.params.id).then(g => {
        res.render('./game/edit', { game: g });
    }).catch(ex => {
        console.log(ex);
    });
});

router.post('/edit', isUserLoged, function (req, res) {
    Game.findByPk(req.body.id).then(g => {
        g.gameName = req.body.gameName;
        g.save().then(() => { res.redirect("/game/user/all") });
    }).catch(ex => {
        console.log(ex);
    });
});

router.get('/delete', isUserLoged, function (req, res) {
    Game.findByPk(req.params.id).then(g => {
        g.destroy();
        res.redirect('/game/user/all');
    }).catch(ex => {
        console.log(ex);
    });
});
/* #endregion */

module.exports = router;
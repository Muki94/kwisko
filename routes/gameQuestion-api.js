const express = require('express');
var router = express.Router();
const uuid = require('uuid/v4');

const GameQuestion = require('./../models').gameQuestion;
const isUserLoged = require('./../helpers/user-auth-helper').isUserLoged;


//GET ALL
router.get('/', isUserLoged, function (req, res) {
    GameQuestion.findAll().then(gq => {
        console.dir(gq);
        res.send(gq);
    }).catch(ex => {
        console.log(ex);
    });
});

router.post('/addQuestionToGame', isUserLoged, function (req, res) {
    GameQuestion.findAll({
        where: {
            'gameId': req.body.gameId,
            'questionId': req.body.questionId
        }
    }).then(gq => {
        if (gq && gq.length > 0)
            res.send("ERROR");
        else {
            let gameQuestion = {
                id: uuid(),
                gameId: req.body.gameId,
                questionId: req.body.questionId
            };

            GameQuestion.create(gameQuestion).then(result => {
                res.send("SUCCESS");
            }).catch(ex => {
                console.log(ex);
                res.send("ERROR");
            });
        }
    }).catch(ex => {
        console.log(ex);
    });
});

router.post('/removeQuestionFromGame', isUserLoged, function (req, res) {
    GameQuestion.findAll({
        where: {
            'gameId': req.body.gameId,
            'questionId': req.body.questionId
        }
    }).then(gq => {
        if (!gq && gq.length == 0)
            res.send("ERROR");
        else {
            gq[0].destroy();
            res.send("SUCCESS");
        }
    }).catch(ex => {
        console.log(ex);
    });
});

module.exports = router;
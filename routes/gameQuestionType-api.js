const express = require('express');
var router = express.Router();

const GameQuestionType = require('./../models').gameQuestionType;
const isUserLoged = require('./../helpers/user-auth-helper').isUserLoged;

//GET ALL
router.get('/', isUserLoged, function (req, res) {
    GameQuestionType.findAll().then(gqt => {
        console.dir(gqt);
        res.send(gqt);
    }).catch(ex => {
        console.log(ex);
    });
});
//GET SINGLE
//ADD
//UPDATE
//DELETE

module.exports = router;
const express = require('express');
var router = express.Router();

const GameTeam = require('./../models').gameTeam;
const isUserLoged = require('./../helpers/user-auth-helper').isUserLoged;

//GET ALL
router.get('/', isUserLoged, function (req, res) {
    GameTeam.findAll().then(gt => {
        console.dir(gt);
        res.send(gt);
    }).catch(ex => {
        console.log(ex);
    });
});

//GET SINGLE
//ADD
//UPDATE
//DELETE

module.exports = router;
const express = require('express');
var router = express.Router();
const isUserLoged = require('./../helpers/user-auth-helper').isUserLoged;

router.get('/', isUserLoged, function (req, res) {
    res.render('./home/index');
});

module.exports = router;
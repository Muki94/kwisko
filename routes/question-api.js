const express = require('express');
const uuid = require('uuid/v4');
var router = express.Router();

const Question = require('./../models').question;
const QuestionType = require('./../models').questionType;
const sequelize = require('./../models').sequelize;

const isUserLoged = require('./../helpers/user-auth-helper').isUserLoged;

/* #region  Handlebars */
router.get('/user/all', isUserLoged, function (req, res) {
    Question.findAll({
        where: {
            userId: req.user
        }
    }).then(q => {
        QuestionType.findAll({
            include: [{
                model: Question,
                attributes: ['id',
                    'questionText',
                    'questionAnswer',
                    'questionNote',
                    [sequelize.fn('TO_CHAR', sequelize.col('questions.createdAt'), 'DD-MM-YYYY HH:mm:ss'), 'createdAt'],
                    [sequelize.fn('TO_CHAR', sequelize.col('questions.updatedAt'), 'DD-MM-YYYY HH:mm:ss'), 'updatedAt']],
                required: false
            }]
        }).then(qtq => {
            res.render('./question/user_questions', { questionTypeQuestions: qtq });
        }).catch(ex => {
            console.log(ex);
        });
    }).catch(ex => {
        console.log(ex);
    });
});

router.get('/detail/:id', isUserLoged, function (req, res) {
    Question.findOne({
        where: {
            'id': req.params.id
        },
        include: [{
            model: QuestionType,
            attributes: ['id', 'questionTypeName']
        }],
        attributes: ['id',
            'questionText',
            'questionAnswer',
            'questionNote',
            [sequelize.fn('TO_CHAR', sequelize.col('question.createdAt'), 'DD-MM-YYYY HH:mm:ss'), 'createdAt'],
            [sequelize.fn('TO_CHAR', sequelize.col('question.updatedAt'), 'DD-MM-YYYY HH:mm:ss'), 'updatedAt']]
    }).then(q => {
        res.render('./question/detail', {
            question: {
                'id': q.id,
                'questionText': q.questionText,
                'questionAnswer': q.questionAnswer,
                'questionNote': q.questionNote,
                'createdAt': q.createdAt,
                'updatedAt': q.updatedAt,
                'questionTypeName': q.questionType.questionTypeName
            }
        });
    }).catch(ex => {
        console.log(ex);
    });
});

router.get('/add', isUserLoged, function (req, res) {
    QuestionType.findAll().then(qt => {
        res.render('./question/add', { questionTypes: qt });
    }).catch(ex => {
        console.log(ex);
    });
});

router.post('/add', isUserLoged, function (req, res) {
    var newQuestion = {
        id: uuid(),
        questionText: req.body.questionText,
        questionAnswer: req.body.questionAnswer,
        questionNote: req.body.questionNote,
        questionTypeId: req.body.questionType,
        userId: req.user
    };

    Question.create(newQuestion).then(r => {
        res.redirect("/question/user/all");
    }).catch(ex => {
        console.log(ex);
    });
});

router.get('/edit/:id', isUserLoged, function (req, res) {
    Question.findByPk(req.params.id).then(q => {
        QuestionType.findAll().then(qt => {
            res.render('./question/edit', { question: q, questionTypes: qt });
        }).catch(ex => {
            console.log(ex);
        });
    }).catch(ex => {
        console.log(ex);
    });
});

router.post('/edit', isUserLoged, function (req, res) {
    Question.findByPk(req.body.id).then(q => {
        q.questionText = req.body.questionText;
        q.questionAnswer = req.body.questionAnswer;
        q.questionNote = req.body.questionNote;
        q.questionTypeId = req.body.questionType;

        q.save().then(() => {
            res.redirect("/question/user/all")
        }).catch(ex => {
            console.log(ex)
        });
    }).catch(ex => {
        console.log(ex);
    });
});

router.get('/delete/:id', isUserLoged, function (req, res) {
    Question.findByPk(req.params.id).then(q => {
        q.destroy();
        res.redirect('/question/user/all');
    }).catch(ex => {
        console.log(ex);
    });
});
/* #endregion */
module.exports = router;
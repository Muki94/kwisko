const express = require('express');
var router = express.Router();

const QuestionType = require('./../models').questionType;
const isUserLoged = require('./../helpers/user-auth-helper').isUserLoged;

//GET ALL
router.get('/', isUserLoged, function (req, res) {
    QuestionType.findAll().then(qt => {
        console.dir(qt);
        res.send(qt);
    }).catch(ex => {
        console.log(ex);
    });
});

//GET SINGLE
router.get('/:questionTypeId', isUserLoged, function (req, res) {
    QuestionType.findByPk(req.params.questionTypeId).then(qt => {
        console.dir(qt);
        res.send(qt);
    }).catch(ex => {
        console.log(ex);
    });
});

module.exports = router;
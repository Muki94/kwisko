const express = require('express');
var router = express.Router();
const uuid = require('uuid/v4');

const Team = require('./../models').team;
const isUserLoged = require('./../helpers/user-auth-helper').isUserLoged;

//GET ALL
router.get('/', isUserLoged, function (req, res) {
    Team.findAll().then(t => {
        console.dir(t);
        res.send(t);
    }).catch(ex => {
        console.log(ex);
    });
});

//GET SINGLE
router.get('/:teamId', isUserLoged, function (req, res) {
    Team.findByPk(req.params.teamId).then(t => {
        console.dir(t);
        res.send(t);
    }).catch(ex => {
        console.log(ex);
    });
});

//ADD
router.post('/', isUserLoged, function (req, res, next) {
    var newTeam = {
        id: uuid(),
        teamName: req.body.teamName
    };

    Team.create(newTeam).then(r => {
        res.send("new team added");
    }).catch(ex => {
        console.log(ex);
    });
});

//UPDATE
router.put('/:teamId', isUserLoged, function (req, res, next) {
    Team.findByPk(req.params.teamId).then(t => {
        t.update({ teamName: req.body.teamName }).then(r => {
            res.send(`team with id ${req.params.teamId} updated`);
        }).catch(ex => {
            console.log(ex);
        });
    }).catch(ex => {
        console.log(ex);
    })
});

//DELETE
router.delete('/:teamId', isUserLoged, function (req, res, next) {
    Team.findByPk(req.params.teamId).then(t => {
        t.destroy();
        res.send(`team with id ${req.params.teamId} deleted`);
    }).catch(ex => {
        console.log(ex);
    });
});

module.exports = router;
const express = require('express');
var router = express.Router();
const passport = require('passport');

const User = require('./../models').user;
const isUserLoged = require('./../helpers/user-auth-helper').isUserLoged;

/* #region  Handlebars */
//GET ALL
router.get('/all', isUserLoged, function (req, res) {
    User.findAll().then(u => {
        res.render('./user/index', { users: u });
    }).catch(ex => {
        console.log(ex);
    });
});

//GET SINGLE
router.get('/single', isUserLoged, function (req, res) {
    User.findByPk(req.query.userId).then(u => {
        res.render('./user/details', { user: u });
    }).catch(ex => {
        console.log(ex);
    });
});

router.get('/login', function (req, res) {
    res.render('./user/login', { layout: false });
});

router.post('/authenticate', function (req, res, next) {
    passport.authenticate('local', {
        successRedirect: '/home',
        failureRedirect: '/user/login'
    })(req, res, next);
});

router.get('/logout', function (req, res, next) {
    req.logout().destroy(function (err) {
        if (!err)
            res.redirect('/user/login');

        res.send(err);
    });
})
/* #endregion */

module.exports = router;
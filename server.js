const express = require('express');
const exphbs = require('express-handlebars');
const port = process.env.port || 3000;
const passport = require('passport')
const bodyParser = require('body-parser');
const session = require('express-session');
const path = require('path');

const app = express();

//passport config
require('./config/passport')(passport);

let hbs = exphbs.create({
    defaultLayout: 'main',
    layoutsDir: path.join(__dirname, 'views/layouts'),
    partialsDir: path.join(__dirname, 'views/partials'),
    helpers: {
        select: function (selected, options) {
            return options.fn(this).replace(
                new RegExp(' value=\"' + selected + '\"'),
                '$& selected="selected"');
        },
        switch: function (value, options) {
            this.switch_value = value;
            this.switch_break = false;
            return options.fn(this);
        },
        case: function (value, options) {
            if (value == this.switch_value) {
                this.switch_break = true;
                return options.fn(this);
            }
        },
        default: function (value, options) {
            if (this.switch_break == false) {
                return value;
            }
        },
        if_equal: function (a, b, opts) {
            if (a == b) {
                return opts.fn(this)
            } else {
                return opts.inverse(this)
            }
        }
    }
});

//view engine setup 
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

//midleware
app.use(express.static(__dirname + '/public'));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(session({
    secret: 'anything',
    resave: false,
    saveUninitialized: false
    //cookie: { secure: true }
}));
app.use(passport.initialize());
app.use(passport.session());

//include routes 
app.use('/home', require('./routes/home-api'));
app.use('/game', require('./routes/game-api'));
app.use('/team', require('./routes/team-api'));
app.use('/user', require('./routes/user-api'));
app.use('/questionType', require('./routes/questionType-api'));
app.use('/question', require('./routes/question-api'));
app.use('/gameQuestion', require('./routes/gameQuestion-api'));
app.use('/gameQuestionType', require('./routes/gameQuestionType-api'));
app.use('/gameTeam', require('./routes/gameTeam-api'));

//first page to render
app.get('/', function (req, res) {
    if (!req.user)
        res.redirect('./user/login');

    res.redirect('./home');
});

app.listen(port, function () {
    console.log(`Listening app on port ${port}`);
})